import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:get_it/get_it.dart';
import 'package:password_crypt/static/app_string.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:win32/win32.dart';

import '../blocks/main/data/password_controller.dart';

final getIt = GetIt.instance;

class App {
  static Future<void> initApp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!getIt.isRegistered<SharedPreferences>()) {
      getIt.registerLazySingleton<SharedPreferences>(() => prefs);
    }

    if (!getIt.isRegistered<PasswordController>()) {
      getIt.registerLazySingleton<PasswordController>(
          () => PasswordController());
    }
  }

  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  static String encrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    return encrypted.base64;
  }

  static String decrypt(String text, String password) {
    final key = Key.fromUtf8(generateMd5(password));
    final iv = IV.fromLength(16);
    final encrypter = Encrypter(AES(key));
    final encrypted = Encrypted.fromBase64(text);
    final content = encrypter.decrypt(encrypted, iv: iv);
    return content;
  }

  static List<String>? get credentialList =>
      getIt<SharedPreferences>().getStringList(generateMd5(AppString.array));

  static set credentialList(List<String>? list) {
    getIt<SharedPreferences>()
        .setStringList(generateMd5(AppString.array), list!);
  }
  static String get computerName {
    final nameLength = calloc<DWORD>();
    String name;

    GetComputerNameEx(
        COMPUTER_NAME_FORMAT.ComputerNameDnsFullyQualified, nullptr, nameLength);

    final namePtr = wsalloc(nameLength.value);

    try {
      final result = GetComputerNameEx(
          COMPUTER_NAME_FORMAT.ComputerNameDnsFullyQualified,
          namePtr,
          nameLength);

      if (result != 0) {
        name = namePtr.toDartString();
      } else {
        throw WindowsException(HRESULT_FROM_WIN32(GetLastError()));
      }
    } finally {
      free(namePtr);
      free(nameLength);
    }
    return name;
  }
}
