import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:password_crypt/blocks/camera/screens/camera.dart';
import 'blocks/loading/screens/loading_screen.dart';
import 'package:win32/win32.dart';
import 'package:shell/shell.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // MethodChannel channel = const MethodChannel('method_channel');
  // bool resultIntegrity = await channel.invokeMethod('integrity');
  // print("selfIntegrity = " + resultIntegrity.toString());
  // if (resultIntegrity == false) {
  //   final message = TEXT('This app was patched');
  //   final title = TEXT('There is not original app');
  //   MessageBox(
  //       NULL,
  //       message,
  //       title,
  //       MB_ICONASTERISK | // Warning
  //           MB_DEFBUTTON2 // Second button is the default
  //       );
  // }
  // bool resultDebugger = await channel.invokeMethod('debugger');
  // print("selfDebugger = " + resultDebugger.toString());

  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
    home: LoadingScreen(),
  ));
}
