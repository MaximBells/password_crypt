class Credential {
  Credential.fromString(String row) {
    List<String> data = row.split('\$');
    icon = data[0];
    url = data[1];
    login = data[2];
    password = data[3];
    date = DateTime.parse(data[4]);
  }

  Credential(
      {required this.icon,
      required this.url,
      required this.login,
      required this.password,
      required this.date});

  String icon = "";
  String url = "";
  String login = "";
  String password = "";

  DateTime date = DateTime.now();

  @override
  String toString() {
    return '$icon\$$url\$$login\$$password\$$date';
  }
}
