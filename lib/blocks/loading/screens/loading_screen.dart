import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:password_crypt/blocks/auth/screens/auth_screen.dart';
import 'package:password_crypt/blocks/main/data/password_controller.dart';
import 'package:password_crypt/blocks/main/screens/main_screen.dart';
import 'package:password_crypt/static/app.dart';

import '../../camera/screens/camera.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReactionBuilder(
        child: Scaffold(
          body: Center(
            child: Bounce(
                duration: const Duration(milliseconds: 1000),
                delay: const Duration(seconds: 1),
                infinite: true,
                child: FlutterLogo(
                  size: MediaQuery.of(context).size.width * 0.35,
                )),
          ),
        ),
        builder: (context) {
          return reaction((_) {
            App.initApp();
            Future.delayed(const Duration(seconds: 3)).then((value) {
              getIt<PasswordController>().setAuthType(AuthType.enter);
              Navigator.pushReplacement(context,
                  CupertinoPageRoute(builder: (context) => const MyApp()));
              // if (App.credentialList == null || App.credentialList!.isEmpty) {
              //   getIt<PasswordController>().setAuthType(AuthType.create);
              //   Navigator.pushReplacement(
              //       context,
              //       CupertinoPageRoute(
              //           builder: (context) => const MainScreen()));
              // } else {
              //   getIt<PasswordController>().setAuthType(AuthType.enter);
              //   Navigator.pushReplacement(context,
              //       CupertinoPageRoute(builder: (context) => AuthScreen()));
              // }
            });
          }, (result) {});
        });
  }
}
