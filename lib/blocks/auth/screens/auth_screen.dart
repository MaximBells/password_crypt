import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:password_crypt/blocks/camera/screens/camera.dart';
import 'package:password_crypt/blocks/main/data/password_controller.dart';
import 'package:password_crypt/blocks/main/widgets/custom_button.dart';
import 'package:password_crypt/static/app.dart';

class AuthScreen extends StatelessWidget {
  AuthScreen({Key? key}) : super(key: key);

  final TextEditingController controller = TextEditingController();

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      return Form(
        key: formKey,
        child: Scaffold(
          backgroundColor: Colors.blue,
          appBar: AppBar(
            title: Text(getIt<PasswordController>().authTitle),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        CupertinoPageRoute(
                            builder: (context) => const MyApp()));
                  },
                  child: const Text(
                    'Войти по фото',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
            ],
            centerTitle: true,
          ),
          body: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.all(32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(24),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(1, 5), // changes position of shadow
                        ),
                      ]),
                  child: Column(
                    children: [
                      Text(getIt<PasswordController>().authBody),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.33,
                        child: TextFormField(
                          decoration: const InputDecoration(
                              hintText: 'Enter PIN code...'),
                          controller: controller,
                          validator: (value) =>
                              getIt<PasswordController>().checkAuth(value),
                        ),
                      ),
                      Observer(builder: (context) {
                        return CustomButton(
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                getIt<PasswordController>().pressAuthButton(
                                    buildContext: context,
                                    value: controller.text);
                              }
                            },
                            title: getIt<PasswordController>().authTitle,
                            isLoading: getIt<PasswordController>().isLoading);
                      }),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
/*
ScaffoldMessenger.of(context)
                                    .showSnackBar(const SnackBar(
                                  content: Text('OK'),
                                  backgroundColor: Colors.green,
                                ));
 */
