import 'package:flutter/material.dart';
import 'package:password_crypt/static/app_string.dart';

class EmptyRow extends StatelessWidget {
  const EmptyRow({Key? key}) : super(key: key);

  static const TextStyle textStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 24);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Expanded(
              child: Text(
            AppString.site,
            style: textStyle,
          )),
          Expanded(
              child: Text(
            AppString.login,
            style: textStyle,
          )),
          Expanded(
              child: Text(
            AppString.password,
            style: textStyle,
          )),
        ],
      ),
    );
  }
}
