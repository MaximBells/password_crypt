// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PasswordController on _PasswordController, Store {
  Computed<String>? _$authTitleComputed;

  @override
  String get authTitle =>
      (_$authTitleComputed ??= Computed<String>(() => super.authTitle,
              name: '_PasswordController.authTitle'))
          .value;
  Computed<String>? _$authBodyComputed;

  @override
  String get authBody =>
      (_$authBodyComputed ??= Computed<String>(() => super.authBody,
              name: '_PasswordController.authBody'))
          .value;

  late final _$isLoadingAtom =
      Atom(name: '_PasswordController.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$pinCodeAtom =
      Atom(name: '_PasswordController.pinCode', context: context);

  @override
  String? get pinCode {
    _$pinCodeAtom.reportRead();
    return super.pinCode;
  }

  @override
  set pinCode(String? value) {
    _$pinCodeAtom.reportWrite(value, super.pinCode, () {
      super.pinCode = value;
    });
  }

  late final _$authTypeAtom =
      Atom(name: '_PasswordController.authType', context: context);

  @override
  AuthType get authType {
    _$authTypeAtom.reportRead();
    return super.authType;
  }

  @override
  set authType(AuthType value) {
    _$authTypeAtom.reportWrite(value, super.authType, () {
      super.authType = value;
    });
  }

  late final _$credentialListAtom =
      Atom(name: '_PasswordController.credentialList', context: context);

  @override
  ObservableList<Credential> get credentialList {
    _$credentialListAtom.reportRead();
    return super.credentialList;
  }

  @override
  set credentialList(ObservableList<Credential> value) {
    _$credentialListAtom.reportWrite(value, super.credentialList, () {
      super.credentialList = value;
    });
  }

  late final _$createCredentialAsyncAction =
      AsyncAction('_PasswordController.createCredential', context: context);

  @override
  Future<void> createCredential(
      {required String url,
      required String login,
      required String password,
      required BuildContext context}) {
    return _$createCredentialAsyncAction.run(() => super.createCredential(
        url: url, login: login, password: password, context: context));
  }

  late final _$_PasswordControllerActionController =
      ActionController(name: '_PasswordController', context: context);

  @override
  void initCredentialList() {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.initCredentialList');
    try {
      return super.initCredentialList();
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearCredentialList() {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.clearCredentialList');
    try {
      return super.clearCredentialList();
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addCredential({required Credential credential}) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.addCredential');
    try {
      return super.addCredential(credential: credential);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeCredential({required Credential credential}) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.removeCredential');
    try {
      return super.removeCredential(credential: credential);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateCredentialList() {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.updateCredentialList');
    try {
      return super.updateCredentialList();
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void saveCredentialList({required Credential credential}) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.saveCredentialList');
    try {
      return super.saveCredentialList(credential: credential);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void sortCredentialList() {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.sortCredentialList');
    try {
      return super.sortCredentialList();
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void pressAddCredential(BuildContext buildContext) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.pressAddCredential');
    try {
      return super.pressAddCredential(buildContext);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void pressAuthButton(
      {required BuildContext buildContext, required String value}) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.pressAuthButton');
    try {
      return super.pressAuthButton(buildContext: buildContext, value: value);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? checkAuth(String? value) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.checkAuth');
    try {
      return super.checkAuth(value);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? checkSite(String? value) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.checkSite');
    try {
      return super.checkSite(value);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? checkLogin(String? value) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.checkLogin');
    try {
      return super.checkLogin(value);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? checkPassword(String? value) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.checkPassword');
    try {
      return super.checkPassword(value);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setAuthType(AuthType type) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.setAuthType');
    try {
      return super.setAuthType(type);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void copyToClipBoard({required String text, required BuildContext context}) {
    final _$actionInfo = _$_PasswordControllerActionController.startAction(
        name: '_PasswordController.copyToClipBoard');
    try {
      return super.copyToClipBoard(text: text, context: context);
    } finally {
      _$_PasswordControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
pinCode: ${pinCode},
authType: ${authType},
credentialList: ${credentialList},
authTitle: ${authTitle},
authBody: ${authBody}
    ''';
  }
}
