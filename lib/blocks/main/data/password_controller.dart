import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
import 'package:password_crypt/blocks/auth/screens/auth_screen.dart';
import 'package:password_crypt/blocks/main/screens/create_credential.dart';
import 'package:password_crypt/blocks/main/screens/main_screen.dart';
import 'package:password_crypt/models/credential.dart';
import 'package:password_crypt/static/app.dart';
import 'package:password_crypt/static/app_string.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:favicon/favicon.dart' as favicon;

part 'password_controller.g.dart';

enum AuthType { create, enter }

class PasswordController extends _PasswordController with _$PasswordController {
}

abstract class _PasswordController with Store {
  @observable
  bool isLoading = false;

  @observable
  String? pinCode;

  @observable
  AuthType authType = AuthType.enter;

  @observable
  ObservableList<Credential> credentialList = ObservableList.of([]);

  @computed
  String get authTitle {
    switch (authType) {
      case AuthType.create:
        return AppString.createPinCode;

      case AuthType.enter:
        return AppString.enterPinCode;
    }
  }

  @computed
  String get authBody {
    switch (authType) {
      case AuthType.create:
        return AppString.toCreateCredential;

      case AuthType.enter:
        return AppString.toEnterCredential;
    }
  }

  @action
  void initCredentialList() {
    List<String> savedArray = App.credentialList ?? [];
    if (savedArray.isNotEmpty) {
      for (var element in savedArray) {
        credentialList
            .add(Credential.fromString(App.decrypt(element, pinCode!)));
      }
      sortCredentialList();
    }
  }

  @action
  void clearCredentialList() {
    credentialList.clear();
  }

  @action
  void addCredential({required Credential credential}) {
    credentialList.add(credential);
    sortCredentialList();
    saveCredentialList(credential: credential);
  }

  @action
  void removeCredential({required Credential credential}) {
    credentialList.remove(credential);
    sortCredentialList();
    saveCredentialList(credential: credential);
  }

  @action
  void updateCredentialList() => credentialList = credentialList;

  @action
  void saveCredentialList({required Credential credential}) {
    List<String> savedCredentialList = [];
    for (var element in credentialList) {
      savedCredentialList.add(App.encrypt(element.toString(), pinCode!));
    }
    App.credentialList = savedCredentialList;
  }

  @action
  void sortCredentialList() =>
      credentialList.sort((a, b) => b.date.compareTo(a.date));

  @action
  void pressAddCredential(BuildContext buildContext) {
    if (pinCode == null) {
      authType = AuthType.create;
      Navigator.push(
          buildContext, CupertinoPageRoute(builder: (context) => AuthScreen()));
    } else {
      Navigator.push(buildContext,
          CupertinoPageRoute(builder: (context) => CreateCredential()));
    }
  }

  @action
  void pressAuthButton(
      {required BuildContext buildContext, required String value}) {
    pinCode = App.generateMd5(value);
    if (authType == AuthType.create) {
      Navigator.pushReplacement(buildContext,
          CupertinoPageRoute(builder: (context) => CreateCredential()));
    } else if (authType == AuthType.enter) {
      initCredentialList();
      Navigator.pushReplacement(buildContext,
          CupertinoPageRoute(builder: (context) => const MainScreen()));
    }
  }

  @action
  String? checkAuth(String? value) {
    switch (authType) {
      case AuthType.create:
        if (value == null || value.isEmpty) {
          return 'Cant be empty';
        }
        if (value.length != 4) {
          return 'Length must be 4 ';
        }
        return null;
      case AuthType.enter:
        if (value == null || value.isEmpty) {
          return 'Cant be empty';
        }
        try {
          App.decrypt(App.credentialList!.first, App.generateMd5(value));
          return null;
        } catch (e) {
          return 'Wrong PIN code';
        }
    }
  }

  @action
  String? checkSite(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (Uri.tryParse(value) == null) {
      return 'Must be a real URL';
    }
    return null;
  }

  @action
  String? checkLogin(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (value.length < 2) {
      return 'Length must be more than 2';
    }
    return null;
  }

  @action
  String? checkPassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Cant be empty';
    }
    if (value.length < 2) {
      return 'Length must be more than 2';
    }
    return null;
  }

  @action
  Future<void> createCredential(
      {required String url,
      required String login,
      required String password,
      required BuildContext context}) async {
    if (!(await canLaunchUrlString(url))) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        action: SnackBarAction(
          label: 'OKAY',
          textColor: Colors.white,
          onPressed: () {
            // Some code to undo the change.
          },
        ),
        content: const Text(
          'Must be a real url',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
        backgroundColor: Colors.red,
      ));
    } else {
      try {
        isLoading = true;
        var icon = await loadIcon(url);
        addCredential(
            credential: Credential(
                icon: icon!,
                url: url,
                login: App.encrypt(login, pinCode!),
                password: App.encrypt(password, pinCode!),
                date: DateTime.now()));
        isLoading = false;
        Navigator.pop(context);
      } catch (e) {
        isLoading = false;
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          action: SnackBarAction(
            label: 'OKAY',
            textColor: Colors.white,
            onPressed: () {
              // Some code to undo the change.
            },
          ),
          content: Text(
            e.toString(),
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          backgroundColor: Colors.red,
        ));
      }
    }
  }

  Future<String?> loadIcon(String url) async {
    var iconData = await favicon.Favicon.getBest(url);
    if (iconData != null) {
      return iconData.url;
    } else {
      return null;
    }
  }

  @action
  void setAuthType(AuthType type) => authType = type;

  @action
  void copyToClipBoard({required String text, required BuildContext context}) {
    Clipboard.setData(ClipboardData(text: App.decrypt(text, pinCode!)))
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(milliseconds: 1500),
            backgroundColor: Colors.green,
            action: SnackBarAction(
              label: 'Okay',
              onPressed: () {},
            ),
            content: const Text(
              'Copied!',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ))));
  }
}
