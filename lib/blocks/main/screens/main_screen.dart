import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:password_crypt/blocks/auth/screens/auth_screen.dart';
import 'package:password_crypt/blocks/main/data/password_controller.dart';
import 'package:password_crypt/blocks/main/widgets/credential_row.dart';
import 'package:password_crypt/blocks/main/widgets/empty_row.dart';
import 'package:password_crypt/models/credential.dart';
import 'package:password_crypt/static/app.dart';
import 'package:password_crypt/static/app_string.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  /*
ConvexButton.fab(
        size: 128,
        iconSize: 36,
        sigma: 12,
        border: 4,
        thickness: 24,
        color: Colors.blue,
        icon: Icons.add,
        onTap: () => getIt<PasswordController>().pressAddCredential(context),
      )
   */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Credentials',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        ),
      ),
      bottomNavigationBar: ConvexAppBar(
          style: TabStyle.titled,
          items: const [TabItem(icon: Icons.add, title: 'Create credential')],
          initialActiveIndex: 0,
          onTap: (int i) =>
              getIt<PasswordController>().pressAddCredential(context)),
      body: Container(
        margin: const EdgeInsets.only(top: 24),
        child: Observer(
          builder: (context) {
            if (getIt<PasswordController>().credentialList.isEmpty) {
              return const EmptyRow();
            } else {
              return ListView.builder(
                  itemCount: getIt<PasswordController>().credentialList.length,
                  itemBuilder: (context, index) {
                    return index == 0
                        ? Column(
                            children: [
                              const EmptyRow(),
                              CredentialRow(
                                  credential: getIt<PasswordController>()
                                      .credentialList[index]),
                              // const Divider(),
                            ],
                          )
                        : Column(
                            children: [
                              CredentialRow(
                                  credential: getIt<PasswordController>()
                                      .credentialList[index]),
                              // const Divider()
                            ],
                          );
                  });
            }
          },
        ),
      ),
    );
  }
}
