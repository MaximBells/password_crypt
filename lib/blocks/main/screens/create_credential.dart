import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:password_crypt/blocks/main/data/password_controller.dart';
import 'package:password_crypt/blocks/main/widgets/custom_button.dart';
import 'package:password_crypt/static/app.dart';

class CreateCredential extends StatelessWidget {
  CreateCredential({Key? key}) : super(key: key);

  final formKey = GlobalKey<FormState>();

  final url = TextEditingController();

  final login = TextEditingController();

  final password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Creating credential',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
        ),
        body: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.all(32),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(24),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(1, 5), // changes position of shadow
                      ),
                    ]),
                child: Column(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.33,
                      child: Observer(builder: (context) {
                        return TextFormField(
                          controller: url,
                          enabled: !getIt<PasswordController>().isLoading,
                          validator: (value) =>
                              getIt<PasswordController>().checkSite(value),
                          decoration:
                              const InputDecoration(hintText: 'Enter site...'),
                        );
                      }),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.33,
                      child: Observer(builder: (context) {
                        return TextFormField(
                          controller: login,
                          enabled: !getIt<PasswordController>().isLoading,
                          validator: (value) =>
                              getIt<PasswordController>().checkLogin(value),
                          decoration:
                              const InputDecoration(hintText: 'Enter login...'),
                        );
                      }),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.33,
                      child: Observer(
                        builder: (context) {
                          return TextFormField(
                            controller: password,
                            enabled: !getIt<PasswordController>().isLoading,
                            validator: (value) =>
                                getIt<PasswordController>().checkPassword(value),
                            decoration: const InputDecoration(
                                hintText: 'Enter password...'),
                          );
                        }
                      ),
                    ),
                    Observer(builder: (context) {
                      return CustomButton(
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              getIt<PasswordController>().createCredential(
                                  url: url.text,
                                  login: login.text,
                                  password: password.text,
                                  context: context);
                            }
                          },
                          title: 'Create credential',
                          isLoading: getIt<PasswordController>().isLoading);
                    })
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
