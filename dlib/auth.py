import io
import logging
import multiprocessing
import os
import pickle
import sys
from operator import itemgetter

import dlib
import numpy as np
from PIL import Image
from telegram import Bot
from telegram.ext import Filters, MessageHandler, Updater

import config

logging.basicConfig(level=config.LOG_LEVEL)

if not os.path.exists(config.LANDMARKS_PATH):
    print(f"{config.LANDMARKS_PATH} does not exist")
    sys.exit(1)

if not os.path.exists(config.MODEL_PATH):
    print(f"{config.MODEL_PATH}  does not exist")
    sys.exit(1)

face_detector = dlib.get_frontal_face_detector()
shape_predictor = dlib.shape_predictor(config.LANDMARKS_PATH)
face_recognition_model = dlib.face_recognition_model_v1(config.MODEL_PATH)

with open(os.path.join(config.ASSETS_DIR, "embeddings.pickle"), "rb") as f:
    star_embeddings = pickle.load(f)




def handle_photo():
    image = Image.open(sys.argv[1])
    image.load()
    image = np.asarray(image)

    face_detects = face_detector(image, 1)
    if not face_detects:
        print("no faces")
    #какой алгоритм
    face = face_detects[0]
    #что такое landmarks
    landmarks = shape_predictor(image, face)
    embedding = face_recognition_model.compute_face_descriptor(image, landmarks)
    embedding = np.asarray(embedding)

    ds = []
    #как делают на продакшене
    for name, emb in star_embeddings:
        distance = np.linalg.norm(embedding - emb)
        ds.append((name, distance))

    print(f"{1 - distance}")
    percents = f"{1 - distance}"
    return percents


handle_photo()
