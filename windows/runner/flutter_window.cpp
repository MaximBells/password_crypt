#include "flutter_window.h"

#include <optional>

#include "flutter/generated_plugin_registrant.h"

#include <iostream>

#include<chrono>

#include<thread>


#include <flutter/binary_messenger.h>

#include <flutter/standard_method_codec.h>

#include <flutter/method_channel.h>

#include <flutter/method_result_functions.h>

#include <vector>

#include <QtCore/qbytearray.h>
#include <QCryptographicHash>
#include <qtextcodec.h>
#include <qdebug.h>
#include <QProcess>


#include <string.h>


void myMessageOutput(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    const char* file = context.file ? context.file : "";
    const char* function = context.function ? context.function : "";
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtInfoMsg:
        fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    }
}
typedef unsigned long long QWORD;

FlutterWindow::FlutterWindow(const flutter::DartProject& project)
    : project_(project) {}

FlutterWindow::~FlutterWindow() {}

    void initMethodChannel(flutter::FlutterEngine* flutter_instance) {
    // name your channel
    const static std::string channel_name("method_channel");
    qInstallMessageHandler(myMessageOutput);
    auto channel =
        std::make_unique<flutter::MethodChannel<>>(
            flutter_instance->messenger(), channel_name,
            &flutter::StandardMethodCodec::GetInstance());

    channel->SetMethodCallHandler(
        [](const flutter::MethodCall<>& call,
    std::unique_ptr<flutter::MethodResult<>> result) {

            // cheack method name called from dart
            if (call.method_name().compare("debugger") == 0) {
                   //self debug
                   QProcess* satelliteProcess = new QProcess();
                   int pid = GetCurrentProcessId();
                   qDebug() << "pid = " << pid;
                   QStringList arguments = { QString::number(pid) };
                   qDebug() << "arguments = " << arguments;
                   satelliteProcess->start("E:\\projects\\password_crypt\\assets\\Dbg.exe", arguments);
                   bool ProtectorStarted = satelliteProcess->waitForStarted(1000);
                   qDebug() << "ProtectorStarted = " << ProtectorStarted;
                   result->Success(ProtectorStarted);
            } else if (call.method_name().compare("integrity") == 0){
                  QWORD moduleBase = (QWORD)GetModuleHandle(NULL);
                  QWORD text_segment_start = moduleBase + 0x1000;
                  PIMAGE_DOS_HEADER pIDH = reinterpret_cast<PIMAGE_DOS_HEADER>(moduleBase);
                  PIMAGE_NT_HEADERS pINH = reinterpret_cast<PIMAGE_NT_HEADERS>(moduleBase + pIDH->e_lfanew);
                  QWORD size_of_text = pINH->OptionalHeader.SizeOfCode;
                  QByteArray text_segment_contents = QByteArray((char*)text_segment_start, size_of_text);
                  QByteArray hash = QCryptographicHash::hash((text_segment_contents), QCryptographicHash::Sha256).toBase64();
                  qDebug() << "hash = " << hash;
                  const QByteArray hash0_base64 = QByteArray("I4FC4OwnsXYGhuQBbaxdyG0ydH1knMolBan0jeae9HE=");
                  bool checkResult = (hash == hash0_base64);              
                  result->Success(checkResult);
            }
            else {
                result->NotImplemented();
            }
        });
     }




bool FlutterWindow::OnCreate() {
  if (!Win32Window::OnCreate()) {
    return false;
  }

  RECT frame = GetClientArea();

  // The size here must match the window dimensions to avoid unnecessary surface
  // creation / destruction in the startup path.
  flutter_controller_ = std::make_unique<flutter::FlutterViewController>(
      frame.right - frame.left, frame.bottom - frame.top, project_);
  // Ensure that basic setup of the controller was successful.
  if (!flutter_controller_->engine() || !flutter_controller_->view()) {
    return false;
  }
  RegisterPlugins(flutter_controller_->engine());
  initMethodChannel(flutter_controller_->engine());

  SetChildContent(flutter_controller_->view()->GetNativeWindow());
  return true;
}

void FlutterWindow::OnDestroy() {
  if (flutter_controller_) {
    flutter_controller_ = nullptr;
  }

  Win32Window::OnDestroy();
}

LRESULT
FlutterWindow::MessageHandler(HWND hwnd, UINT const message,
                              WPARAM const wparam,
                              LPARAM const lparam) noexcept {
  // Give Flutter, including plugins, an opportunity to handle window messages.
  if (flutter_controller_) {
    std::optional<LRESULT> result =
        flutter_controller_->HandleTopLevelWindowProc(hwnd, message, wparam,
                                                      lparam);
    if (result) {
      return *result;
    }
  }

  switch (message) {
    case WM_FONTCHANGE:
      flutter_controller_->engine()->ReloadSystemFonts();
      break;
  }

  return Win32Window::MessageHandler(hwnd, message, wparam, lparam);
}
